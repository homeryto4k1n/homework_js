// Написать функцию createNewUser(), которая будет создавать
// и возвращать объект newUser.
function createNewUser() {
    // При вызове функция должна спросить у вызывающего имя и фамилию.
    // Используя данные, введенные пользователем.
    let userFirstName = prompt("Enter your first name");
    let userLastName = prompt("Enter your last name");

    // Создать объект newUser со свойствами firstName и lastName.
    let newUser = {};
    let _firstName;
    Object.defineProperty(newUser, 'firstName', {
        get: function () {
            return _firstName;
        },
        set: function (firstName) {
            _firstName = firstName;
        },
    });

    let _lastName;
    Object.defineProperty(newUser, 'lastName', {
        get: function () {
            return _lastName;
        },
        set: function (lastName) {
            _lastName = lastName;
        },
    });

    // Добавить в объект newUser метод getLogin(), который будет возвращать
    // первую букву имени пользователя, соединенную с фамилией пользователя,
    // все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
    Object.defineProperty(newUser, 'getLogin', {
        value: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    });

    newUser.firstName = userFirstName;
    newUser.lastName = userLastName;

    return newUser;
}

// Создать пользователя с помощью функции createNewUser().
const user = createNewUser();
// Вызвать у пользователя функцию getLogin().
const login = user.getLogin();
// Вывести в консоль результат выполнения функции.
console.log(login);